HMail - A Mail App That Doesn't Suck
========================================

HMail is a mail application for Firefox OS that doesn't suck. So what does 
it mean for a mail application to not suck? 

1. Conversations:
   HMail will have GMail-style threading. 

2. Support for POP, IMAP and Exchange ActiveSync.
   If a mail app doesn't support the protocol your mail service uses, it sucks.
